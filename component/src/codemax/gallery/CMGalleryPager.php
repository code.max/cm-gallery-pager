<?php
namespace codemax\gallery;

use codemax\wp\plugin\WPPluginEnv;
use Elementor\Core\App\Modules\ImportExport\Directories\Post_Type;

abstract class CMGalleryPager
{
    
    /**
     * 
     * @var WPPluginEnv
     */
    static private $pluginEnv = null;
    
    /**
     *
     * @var array
     */
    static private $pages = [];

    /**
     * 
     * @var string
     */
    static private $featuredPagesCode = '';
    
    /**
     * 
     * @var CMGalleryPagerConfig
     */
    static private $config = NULL;
    
    /**
     *
     */
    static public function install(): void
    {
        self::$pluginEnv = WPPluginEnv::current();
        self::$pluginEnv->prefix( 'front-css' , 'component/front/ui/css/' );
        self::$pluginEnv->prefix( 'front-js' , 'component/front/ui/js/' );

        add_shortcode( 'cm_gallery_pager', [ CMGalleryPager::class, 'shortcode' ] );
        
        add_action( 'wp_enqueue_scripts', [ CMGalleryPager::class, 'addStylesheets' ] );
        add_action( 'wp_enqueue_scripts', [ CMGalleryPager::class, 'addScripts' ] );
    }
    
    /**
     *
     */
    static public function addStylesheets(): void
    {
        wp_enqueue_style(
            'cm-gallery-pager-css',
            self::$pluginEnv->url( 'cm-gallery-pager.css', 'front-css' )
        );
    }
    
    /**
     *
     */
    static public function addScripts(): void
    {
        wp_enqueue_script(
            'cm-gallery-pager-js',
            self::$pluginEnv->url( 'cm-gallery-pager.js', 'front-js' ),
            [ 'jquery' ],
            false,
            true
           );
    }
    
    /**
     *
     * @return string
     */
    static public function templatePath(): string
    {
        return self::$pluginEnv->path( 'cm-gallery-pager-template.php' );
    }
    
    /**
     * 
     * @param CMGalleryPagerConfig $config
     */
    static public function config( CMGalleryPagerConfig $config = NULL ): CMGalleryPagerConfig
    {
        if ( isset( $config ) )
        {
            self::$config = $config;
        }
        return self::$config;
    }    
    
    /**
     * 
     * @return string
     */
    static public function featuredPageCode(): string
    {
        return self::$featuredPagesCode;
    }
    
    /**
     * 
     * @return array
     */
    static public function pages(): array
    {
        return self::$pages;
    }    
    
    /**
     * 
     */
    static private function generate(): void
    {
        if ( self::config() !== null )
        {
            $featuredIds = array_slice( self::config()->list(), 0, self::config()->featuredPageSize() );
            $featuredIds = implode( ', ' , $featuredIds );
            self::$featuredPagesCode = "[gallery ids=\"{$featuredIds}\"]";
            
            $pagesIds = array_chunk( 
                array_slice( self::config()->list(), self::config()->featuredPageSize() ), 
                self::config()->pageSize() 
            );
            foreach ( $pagesIds as $page )
            {
                $ids = implode( ', ', $page );
                self::$pages[] = "[gallery ids=\"{$ids}\"]";
            }
        }
    }
    
    /**
     * @param array $attrs
     *      $attrs[ 'ids' ]: string[] (required)
     *      $attrs[ 'featured-page-size' ]: int (required)
     *      $attrs[ 'page-size' ]: int
     *      $attrs[ 'text-button' ]: string
     * 
     * @return string
     */
    static public function shortcode( array $attrs = [] ): string
    {
        self::$config = CMGalleryPagerConfig::create( $attrs );
        if ( is_object( self::$config ) )
        {
            $code = '';
            self::generate();
            ob_start();
            include self::templatePath();
            $code = ob_get_contents();
            ob_end_clean();
            return $code;
        }
        return '';
    }    
}