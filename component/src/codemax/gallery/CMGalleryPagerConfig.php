<?php
namespace codemax\gallery;

use codemax\wp\db\CMDBConsole;

class CMGalleryPagerConfig
{
    /**
     * 
     * @var array
     */
    private $listIds = [];
    
    /**
     * 
     * @var string
     */
    private $textButton = 'Ver más . . .';
    
    /**
     *
     * @var integer
     */
    private $featuredPageSize = 0;
    
    /**
     *
     * @var integer
     */
    private $pageSize = 0;

    /**
     * 
     * @param array $attrs
     *      $attrs[ 'ids' ]: string[] || $attrs[ 'term' ]: string (required)
     *      $attrs[ 'featured-page-size' ]: int (required)
     *      $attrs[ 'page-size' ]: int
     *      $attrs[ 'text-button' ]: string
     * 
     * @return CMGalleryPagerConfig|NULL
     */
    static public function create( array $attrs = [] )
    {
        if ( !isset( $attrs[ 'ids' ] ) 
            && isset( $attrs[ 'term' ] ) 
            && isset( $attrs[ 'featured-page-size' ] ) )
        {
            $attrs[ 'ids' ] = CMDBConsole::imageIdsByTerm( $attrs[ 'term' ] );
            error_log( print_r( $attrs[ 'ids' ], true ) );
        }
        if ( isset( $attrs[ 'ids' ] ) && isset( $attrs[ 'featured-page-size' ] ) )
        {
            if ( is_string( $attrs[ 'ids' ] ) )
            {
                $attrs[ 'ids' ] = explode( ',' , $attrs[ 'ids' ] );
            }
            $config = new CMGalleryPagerConfig(
                $attrs[ 'ids' ],
                $attrs[ 'featured-page-size' ]
                );
            if ( isset( $attrs[ 'text-button' ] ) )
            {
                $config->textButton( $attrs[ 'text-button' ] );
            }
            if ( isset( $attrs[ 'page-size' ] ) )
            {
                $config->pageSize( $attrs[ 'page-size' ] );
            }
            return $config;
        }
        return NULL;        
    }
    
    /**
     * 
     * @param array $ids
     * @param int $featuredPageSize
     * @param int $pageSize
     * @param string $textButton
     */
    public function __construct( array $ids, int $featuredPageSize, int $pageSize = NULL, string $textButton = NULL )
    {
        $this->listIds = $ids;
        $this->featuredPageSize( $featuredPageSize );
        if ( isset( $pageSize ) )
        {
            $this->pageSize = $pageSize;
        }
        else
        {
            $this->pageSize = $featuredPageSize;
        }
        if ( isset( $textButton ) )
        {
            $this->textButton = $textButton;
        }
    }    

    /**
     *
     * @param array $ids
     * @return array
     */
    public function list( array $ids = null ): array
    {
        if ( isset( $ids ) )
        {
            $this->listIds = $ids;
        }
        return $this->listIds;
    }
      
    /**
     * 
     * @param string $text
     * @return string
     */
    public function textButton( string $text = NULL ): string
    {
        if ( isset( $text ) )
        {
            $this->textButton = $text;
        }
        return $this->textButton;
    }
    
    /**
     * 
     * @param int $size
     * @return int
     */
    public function pageSize( int $size = NULL ): int
    {
        if ( isset( $size ) )
        {
            $this->pageSize = $size;
        }
        return $this->pageSize;
    }
    
    /**
     * 
     * @param int $size
     * @return int
     */
    public function featuredPageSize( int $size = NULL ): int
    {
        if ( isset( $size ) )
        {
            $this->featuredPageSize = $size;
        }
        return $this->featuredPageSize;
    }
}