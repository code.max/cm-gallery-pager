var CM = 
{
	GalleryPager:
	{
		showNextPage: function()
		{
			let query = '.cm-gallery-pager .cm-gp-page[data-state="hide"]';
			let page = jQuery( query );
			if ( page.length > 0 )
			{
				jQuery( query ).first().attr( 'data-state', 'show' );
				if( page.length == 1 )
				{
					jQuery( '.cm-gallery-pager .cm-gp-pager' ).css( 'visibility', 'hidden' );
				}
			}
		}
	},
}

jQuery( document ).ready( function(){
	jQuery( '.cm-gallery-pager .cm-gp-pager button' ).on( 'click', function( event ){
		event.preventDefault();
		CM.GalleryPager.showNextPage();
		event.stopPropagation();
	})
});