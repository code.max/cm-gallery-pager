<?php
require_once __DIR__.'/vendor/autoload.php';

use codemax\distro\Distro;

$distro = new Distro( __DIR__.'/distro-config.json' );

$distro->generate();