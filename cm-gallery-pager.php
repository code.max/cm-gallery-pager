<?php
/** 
 * @wordpress-plugin 
 * Plugin Name: CodeMax | Gallery pager 
 * Description: Wordpress plugin para mostrar una gallería de imágenes con paginado, basado en la galería nativa de WP [gallery].
 * Version: 1.4.2 
 * Author: Codemax 
 * Author URI: https://codemax.com.ar 
 * Text Domain: cm-gallery
 * Requires PHP: 7.4
 * Requires at least: 5.6
 */
defined( 'ABSPATH' ) || exit;

/* Agrega etiquetas a las images */
function wptp_add_tags_to_attachments()
{
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );
}
add_action( 'init' , 'wptp_add_tags_to_attachments' );

require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
codemax\wp\plugin\WPPluginEnv::init();
codemax\gallery\CMGalleryPager::install();