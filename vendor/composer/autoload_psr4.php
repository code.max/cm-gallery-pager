<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'codemax\\wp\\plugin\\' => array($vendorDir . '/codemax/wp-plugin-env/src/codemax/wp/plugin'),
    'codemax\\wp\\db\\' => array($vendorDir . '/codemax/cm-db-console/src/codemax/wp/db'),
    'codemax\\tool\\json\\' => array($vendorDir . '/codemax/tool-json/src/codemax/tool/json'),
    'codemax\\tool\\' => array($vendorDir . '/codemax/tool-path/src/codemax/tool', $vendorDir . '/codemax/tool-txt/src/codemax/tool'),
    'codemax\\gallery\\' => array($baseDir . '/component/src/codemax/gallery'),
    'codemax\\distro\\' => array($vendorDir . '/codemax/generate-distro/src/codemax/distro'),
);
