<?php
namespace codemax\distro;

use codemax\tool\json\entities\models\JsonModel;
use stdClass;

class DistroDestination extends JsonModel
{
    /**
     *
     * @param array|stdClass|null $jsonSource
     */
    public function __construct( $jsonSource = NULL )
    {
        parent::__construct( $jsonSource );
        
        if ( !isset( $this->data()->path ) )
        {
            $this->data()->path = "";
        }
    }
    
    /**
     *
     * @param string $path
     * @return string
     */
    public function path( string $path = null ) : string
    {
        if ( isset( $path ) )
        {
            $this->data()->path = $path;
        }
        return $this->data()->path;
    }
}