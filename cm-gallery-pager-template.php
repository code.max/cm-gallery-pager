<?php
defined( 'ABSPATH' ) || exit;
use codemax\gallery\CMGalleryPager;

$pagesCodes = CMGalleryPager::pages();
?>
<div class="cm-gallery-pager">
	<div class="cm-gp-page-featured">
		<?= do_shortcode( CMGalleryPager::featuredPageCode() ); ?>
	</div>
<?php 
    if ( sizeof( $pagesCodes ) > 0 ) : 
        foreach ( $pagesCodes as $pageCode ): 
?>
    	<div class="cm-gp-page" data-state="hide">
    		<?= do_shortcode( $pageCode ) ?>
    	</div>
<?php 
        endforeach;
?>
	<div class="cm-gp-pager">
		<button>
			<?=  esc_html__( CMGalleryPager::config()->textButton(), 'cm-gallery' ) ?>
		</button> 
	</div>
<?php     
    endif;
?>
</div>